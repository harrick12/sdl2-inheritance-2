#ifndef PLAYER_H_
#define PLAYER_H_

#include "SDL2Common.h"
#include "Sprite.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;

class Player : public Sprite
{
private:
    
    // Animation state
    int state;
        
    // Sprite information
    static const int SPRITE_HEIGHT = 32;
    static const int SPRITE_WIDTH = 24;

    // Bullet spawn
    float cooldownTimer;
    static const float COOLDOWN_MAX;

    Game* game;

    

public:
    Player();
    ~Player();

    // Player Animation states
    enum PlayerState{LEFT=0, RIGHT, UP, DOWN, IDLE};

    void init(SDL_Renderer *renderer);
    void processInput(const Uint8 *keyStates);

    //Overloads
    void update(float timeDeltaInSeconds);

    void setGame(Game* game);
    void fire();
    
    int getCurrentAnimationState();

    void addScore(int points);
    int getScore();

    int points;
};

#endif
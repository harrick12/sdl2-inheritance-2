#include "Player.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "AABB.h"
 

#include <stdexcept>
#include <string>

using std::string;

/**
 * Player
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */

const float Player::COOLDOWN_MAX = 0.2f;

Player::Player() : Sprite()
{
    state = IDLE;
    speed = 50.0f;

    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;

    points = 0;

    health = 50
    maxRange = 1.0f;
    timetoTarget = 0.25f;
}

/**
 * initPlayer
 * 
 * Function to populate an animation structure from given paramters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void Player::init(SDL_Renderer *renderer)
{
    //path string
    string path("assets/images/rpg_sprite_walk.png");

    //postion
    Vector2f position(200.0f,200.0f);

    // Call sprite constructor
    Sprite::init(renderer, path, 5, &position);

    // Setup the animation structure
    animations[LEFT]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[RIGHT]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
    animations[UP]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[DOWN]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.2f);
    }
}

/**
 * ~Player
 * 
 * Destroys the player and any associated 
 * objects 
 * 
 */
Player::~Player()
{

}

/**
 * processInput
 * 
 * Method to process inputs for the player 
 * Note: Need to think about other forms of input!
 * 
 * @param keyStates The keystates array. 
 */
void Player::processInput(const Uint8 *keyStates)
{
    // Process Player Input

    //Input - keys/joysticks?
    float verticalInput = 0.0f;
    float horizontalInput = 0.0f;

    // If no keys are down player should not animate!
    state = IDLE;

    // This could be more complex, e.g. increasing the vertical
    // input while the key is held down.
    if (keyStates[SDL_SCANCODE_UP])
    {
        verticalInput = -1.0f;
        state = UP;
    }

    if (keyStates[SDL_SCANCODE_DOWN])
    {
        verticalInput = 1.0f;
        state = DOWN;
    }

    if (keyStates[SDL_SCANCODE_RIGHT])
    {
        horizontalInput = 1.0f;
        state = RIGHT;
    }

    if (keyStates[SDL_SCANCODE_LEFT])
    {
        horizontalInput = -1.0f;
        state = LEFT;
    }

    aabb = new AABB(this->getPosition(),
        SPRITE_HEIGHT,
        SPRITE_WIDTH);

    // Calculate player velocity.
    velocity->setX(horizontalInput);
    velocity->setY(verticalInput);
    velocity->normalise();
    velocity->scale(speed);

    if (keyStates[SDL_SCANCODE_SPACE])
    {
        fire();
    }
}

int Player::getCurrentAnimationState()
{
    return state;
}

void Player::setGame(Game* game)
{
    this->game = game;
}

void Player::fire()
{
    if (cooldownTimer > COOLDOWN_MAX)
    {
        // Can't fire in idle state!
        if (velocity->length() > 0.0f)
        {
            game->createBullet(position, velocity);
            cooldownTimer = 0;
        }
    }
}
void Player::update(float timeDeltaInSeconds)
{
    Sprite::update(timeDeltaInSeconds);

    cooldownTimer += timeDeltaInSeconds;
}
void Player::addScore(int points)
{

    this->points += points;
}

int Player::getScore()
{
    return points;

}

